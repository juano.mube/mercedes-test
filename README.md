# Mercedes Benz io - Exercise
## Overview
This node project contains my solution to the challenge proposed, summarizing it is cover a flow from the home to a car selection on 'https://www.mercedes-benz.co.uk' application.

Used stack:
+ IDE used: vsCode
+ Language: JavaScript & Node (v19.0.0)
+ Package manager: npm (v8.19.2)
+ Testing FW: Cypress (12.7.0)

## Project structure
The automation was implemented using BDD tests with 3 main logic layers. On this project structure, the folders and files of the default cypress structure are skipped:

```
root
├── cypress
│   ├── e2e 
│   │   ├── features            # Test files
│   │   ├── pages               # Page objects
│   │   └── stepDefinitions     # Gherkin definitions
│   ├── result
│   │   └── result.csv          # Obtained values on the test
|   └── ...
├── cucumber-report.html        # Report generated after test execution 
└── ...
```

### Logic layers
1. Features: Contains the written tests on gherkin language in '.feature' files.
2. Definitions: Connects the gherkin tests with its js implementation, additionally contains the assertions.
3. Pages: Contains the page objects that allows interaction with the web elements.

Depending on the automation complexity, I add a layer between the definitions and the pages, something similar to a 'business actions layer'. Having business-oriented actions improve code reuse, makes easier the maintenance tasks, and makes the project more readable, additionally allows a more organic integration with actions like db queries or api consumptions. For this challenge, adding this layer will not add value but make the project bigger and more complex, so I didn't included it.

## Solution
The required flow was implemented using BDD, and the feature file was written in a way that a complete set of combinations can be tested just adding the test data.

The values of the highest and lowest price, and the name of the screenshot taken on the test execution are saved in the result.csv file.

## Execution
Before execute the test, install the dependencies with the ```npm install``` command.

The tests can be executed in Chrome or Firefox, in headed or headless mode, with the following commands:
+ ```npm run cy:run:headed:chrome```
+ ```npm run cy:run:chrome```
+ ```npm run cy:run:headed:firefox```
+ ```npm run cy:run:firefox```

For these executions, a basic html report is generated on the root as 'cucumber-report.html' and the execution videos are saved on the videos folder.

The cypress UI can be opened using the ```npx cypress open``` or ```npm run cy:open``` commands, on this interface you can decide in which browser execute the tests

## Improvements and next steps
As this is a small project to solve an specific challenge in short time, there are a lot of interesting features that can be implemented to improve the automation

### Improvements
+ Improve the screenshots
+ Share the session between executions to reduce the execution time
+ Define a standarized way to manage the shadows in the html


### Next steps
+ Refactor the test to support the execution in different resolutions (desktop, responsive)
+ Dockerize the test execution.
+ Configure the tests to be executed on a CI/CD tool.
+ Include non-functional verifications, like the used time for generate the QR or check if the BE is throwing errors during the tests.