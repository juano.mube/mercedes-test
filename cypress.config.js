const { defineConfig } = require("cypress");
const { writeFile, access, constants, appendFile} = require('fs')
const path = require('path');

module.exports = defineConfig({
  e2e: {
    viewportWidth: 1920,
    viewportHeight: 1080,
    baseUrl: 'https://www.mercedes-benz.co.uk/',
    specPattern: "**/*.feature",
    trashAssetsBeforeRuns:false,
    defaultCommandTimeout: 20000,
    execTimeout: 180000,
    taskTimeout: 60000,

    async setupNodeEvents(on, config) {
      const createEsbuildPlugin = require('@badeball/cypress-cucumber-preprocessor/esbuild').createEsbuildPlugin
      const createBundler = require('@bahmutov/cypress-esbuild-preprocessor')

      await require('@badeball/cypress-cucumber-preprocessor').addCucumberPreprocessorPlugin(on, config)

      on('file:preprocessor',   createBundler({
        plugins: [createEsbuildPlugin(config)],
      }));

      on('task', {
        log(logMessage){
          console.log(`Log task: "${logMessage}"`);
          return null
        },

        saveResult(data) {

          const resultFolderPath = './cypress/result';
          const resultFilePath = path.join(resultFolderPath, 'result.csv');
          return new Promise((resolve, reject) => {

            access(resultFilePath, constants.F_OK, err => {
              if (err) {
                // If the file doesn't exist, create it with default content
                if (err.code === 'ENOENT') {
                  writeFile(resultFilePath, 'lowerValue,higherValue,screenshot\n', err => {
                    if (err) {
                      console.log(err)
                    }else{
                      console.log(`File ${resultFilePath} created with default content`);
                      appendFile(resultFilePath, data + '\n', err => {
                        if (err) console.log(err);
                        console.log(`Data saved to ${resultFilePath}`);
                        resolve(null)
                      });
                    }

                  });
                }
                resolve(null)
              } else {
                // If the file exists, append the data to it
                appendFile(resultFilePath, data + '\n', err => {
                  if (err) console.log(err);
                  console.log(`Data appended to ${resultFilePath}`);
                  resolve(null)
                });
              }
            });
          })
        },

      })
      return config
    }
  },
});
