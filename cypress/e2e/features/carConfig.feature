Feature: Prices on configuring vehicles

Background:
    Given Im in the mercedes benz home page

@clearExecution
Scenario Outline: Scenario Outline name: Verify price range of vehicles
    When I go to the "<model>" section
    And I select to build a "<vehicleType>" car
    And I choose "<fuelTypes>" on the fuel filter
    Then the price of the vehicles is bewteen "<lowLimitPrice>" and "<highLimitPrice>"
Examples:
|model      |vehicleType    |fuelTypes          |lowLimitPrice  |highLimitPrice |
|Hatchbacks |A-Class        |Diesel             |15000          |60000          |
#|Hatchbacks |B-Class        |Diesel,Premium     |30000          |50000          |