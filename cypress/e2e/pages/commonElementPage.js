export class CommonElementPage{

    webElements = {
        acceptCookiesBtn: () =>  cy.get("cmm-cookie-banner[settings-id='fph8XBqir']").shadow().find('cmm-buttons-wrapper button[data-test="handle-accept-all-button"]'),
    }

    /**
     * Accept the cookies of the page
     */
    acceptCookies(){
        this.webElements.acceptCookiesBtn().click();
    }

}
