export class BuildCarPage{

    getMainShadow(){
        return cy.get("owcc-car-configurator.webcomponent").shadow();
    }

    webElements = {
        fuelTypeExpandable: () => this.getMainShadow().find("ccwb-multi-select").shadow().find('button[tabindex="0"]'),
        fuelTypeCheckboxes: (fuelTypes) => this.getMainShadow().find("ccwb-checkbox").shadow().find(`input[name="${fuelTypes}"]`),
        resultPriceslbls: () => this.getMainShadow().find(`ccwb-text.cc-motorization-header__price span`)
    }

    /**
     * Select the types of fuels on the filter
     * @param {string} fuelTypes should match with the name of the locator on the website
     */
    selectFuelTypes(fuelTypes){
        this.webElements.fuelTypeExpandable().click({ force: true });

        const elements = fuelTypes.split(",");
        for (const element of elements) {
            this.webElements.fuelTypeCheckboxes(element).click({ force: true });
          }

        this.webElements.fuelTypeExpandable().click({ force: true });

    }

        /**
     * Get the values of the visible cars and take an screenshot of them
     * @returns {Array.<String>} Values with the ',' separator and pound symbol
     */
    getCarPrices(){
        const prices=[]

        this.takeScreenshot();

        return this.webElements.resultPriceslbls()
            .each(($els) => {
                prices.push($els.text().replace(' ',''));
            }).then(()=>{
                return prices;
            })
    }

    /**
     * Scroll to the center and take a screenshot. The name of the screenshot is random and is wrapped as 'screenshotName'
     */
    takeScreenshot(){
        const uniqueSeed = Date.now().toString();
        const getUniqueId = () => Cypress._.uniqueId(uniqueSeed);
        const uniqueId = getUniqueId();
        
        cy.scrollTo('center');
        cy.wait(2000);
        cy.screenshot(`${uniqueId}`);
        cy.wrap(uniqueId).as('screenshotName');
    }

    
}