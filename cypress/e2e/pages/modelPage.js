export class ModelPage{

    webElements = {
        buildBtn: () => cy.get("owc-stage").shadow().find('a:contains("Build your car")'),
    }

    /**
     * Click the 'build your car' button
     */
    goToBuildModel(){
        this.webElements.buildBtn().click();
    }


}