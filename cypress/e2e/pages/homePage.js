require('@cypress/xpath');
export class HomePage{
    getSearchShadow(){
        return cy.get("dh-io-vmos.webcomponent").shadow()
    };

    webElements = {
        modelBtns: () => this.getSearchShadow().find('button.dh-io-vmos_1DO_4'),
        typeLinks: () => this.getSearchShadow().find('a[class="dh-io-vmos_2dAQf"]'),

    }

    /**
     * Select the model of the car by the visible text
     * @param {string} model model of the car
     * @returns {boolean} true if the model was available and clicked
     */
    selectModel(model){
        let foundFlag=false
        return this.webElements.modelBtns()
            .each(($els) => {

                if($els.text()===model){
                    foundFlag=true;
                    cy.wrap($els).click();
                    return false;
                }   
            }).then(()=>{
                return foundFlag
            })
    }

        /**
     * Select the vehicle type of the car by the visible text (includes approach)
     * @param {string} vehicleType type of the vehicle
     * @returns {boolean} true if the vehicleType was available and clicked
     */
    selectType(vehicleType){
        let foundFlag=false
        return this.webElements.typeLinks()
            .each(($els) => {
                if($els.text().includes(vehicleType)){
                    foundFlag=true;
                    cy.wrap($els).invoke('attr', 'target', '_self').click();
                    return false;
                }
            }).then(()=>{
                return foundFlag
            })
    }

}