import { Before} from "@badeball/cypress-cucumber-preprocessor"

Before({ tags: "@clearExecution" }, () => {
    cy.clearCookies();
    cy.clearAllLocalStorage()
    cy.clearAllCookies()
    cy.clearAllSessionStorage()
  });