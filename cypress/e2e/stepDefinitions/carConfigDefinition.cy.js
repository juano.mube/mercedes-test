import { Given, When} from "@badeball/cypress-cucumber-preprocessor"
import { CommonElementPage } from "../pages/commonElementPage";
import { HomePage } from "../pages/homePage";
import { ModelPage } from "../pages/modelPage";
import { BuildCarPage } from "../pages/buildCarPage";

const commonElementPage = new CommonElementPage();
const homePage = new HomePage();
const modelPage = new ModelPage();
const buildCarPage = new BuildCarPage();

Given("Im in the mercedes benz home page", () => {
    cy.visit('/');
    commonElementPage.acceptCookies();
})

When("I go to the {string} section", (model) =>{
    homePage.selectModel(model).then((isModelAvailable)=>{
        expect(isModelAvailable).to.equal(true, `the model '${model}' is not available` );
    })

})


When("I select to build a {string} car", (vehicleType) =>{
    homePage.selectType(vehicleType).then((isType)=>{
        expect(isType).to.equal(true, `the model '${isType}' is not available` );
        modelPage.goToBuildModel();
    });


})

When("I choose {string} on the fuel filter", (fuelTypes) =>{
    buildCarPage.selectFuelTypes(fuelTypes);
})

When("the price of the vehicles is bewteen {string} and {string}", (lowLimit,highLimit) =>{
    buildCarPage.getCarPrices().then((carPrices)=>{
        let rawValues = carPrices.map((element) => parseInt(element.replace(/[£,]/g, "")));
        rawValues.sort((a, b) => a - b);       

        cy.get('@screenshotName').then((screenshotName)=>{
            cy.task('saveResult', `${rawValues[0]},${rawValues[rawValues.length-1]},${screenshotName}`)
        })

        expect(rawValues[0]).to.be.at.least(parseInt(lowLimit))
        expect(rawValues[rawValues.length-1]).to.be.at.most(parseInt(highLimit))
    })
})
